namespace Application.Users.Commands.NewAdmin;

public record NewAdminCommand(
    string Username,
    string Password,
    string Email,
    string Phone,
    string FullName,
    string Address) : ICommand<UserResult>;
