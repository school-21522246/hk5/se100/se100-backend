using System.Security.Cryptography;
using Application.Abstractions.Persistence;
using Domain.Common.Shares;
using Domain.Users;
using Domain.Users.Enums;

namespace Application.Users.Commands.NewAdmin;

internal sealed class NewAdminCommandHandler(
        IUserRepository userRepository,
        IUnitOfWork unitOfWork,
        IMapper mapper)
    : ICommandHandler<NewAdminCommand, UserResult>
{
    public async Task<Result<UserResult>> Handle(NewAdminCommand request, CancellationToken cancellationToken)
    {
        if (await userRepository.GetByEmailAsync(request.Email).ConfigureAwait(false) is not null)
        {
            return Result.Failure<UserResult>(DomainException.EmailAlreadyExists);
        }
        if (await userRepository.GetByUsernameAsync(request.Username).ConfigureAwait(false) is not null)
        {
            return Result.Failure<UserResult>(DomainException.UsernameAlreadyExists);
        }
        if (await userRepository.GetRoleAsync(Role.Admin).ConfigureAwait(false) is not { } adminRole)
        {
            throw new InvalidOperationException("Admin role not found");
            // return Result.Failure<AuthenticationResult>(DomainException.rolenotfound);
        }

        using var hashAlgorithm = SHA256.Create();

        var user = User.Create(
            request.Username,
            request.Password,
            request.Email,
            request.Phone,
            request.FullName,
            request.Address);
        user.Roles.Add(adminRole);

        userRepository.Add(user);
        await unitOfWork.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

        return mapper.Map<UserResult>(user);
    }
}
