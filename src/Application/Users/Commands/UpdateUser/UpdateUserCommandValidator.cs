using FluentValidation;

namespace Application.Users.Commands.UpdateUser;

public class UpdateUserCommandValidator : AbstractValidator<UpdateUserCommand>
{
    public UpdateUserCommandValidator()
    {
        // RuleFor(x => x.Username)
        //     .NotEmpty();
        // RuleFor(x => x.Password)
        //     .NotEmpty()
        //     .WithMessage(DomainException.InvalidPassword.Message);
        RuleFor(x => x.Email)
            // .NotEmpty()
            .EmailAddress()
            .WithMessage(DomainException.InvalidEmail.Message);
        RuleFor(x => x.Phone)
            .Matches(@"^[0-9]+$")
            .WithMessage(DomainException.InvalidPhoneNumber.Message)
            .Length(10, 11)
            .WithMessage(DomainException.InvalidPhoneNumber.Message);
        // .NotEmpty();
        RuleFor(x => x.FullName)
            .MaximumLength(20);
        //     .NotEmpty();
        // RuleFor(x => x.Address)
        //     .NotEmpty();
    }
}
