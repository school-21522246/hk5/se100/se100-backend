using Application.Abstractions.Persistence;
using Domain.Common.Shares;
using Domain.Users.ValueObjects;

namespace Application.Users.Commands.UpdateUser;

internal sealed class UpdateUserCommandHandler(
    IUserRepository userRepository,
    IUnitOfWork unitOfWork,
    IMapper mapper
) : ICommandHandler<UpdateUserCommand, UserResult>
{
    public async Task<Result<UserResult>> Handle(
        UpdateUserCommand request,
        CancellationToken cancellationToken
    )
    {
        if (
            await userRepository
                .GetByIdAsync(UserId.Create(request.Id), cancellationToken)
                .ConfigureAwait(false)
            is not { } user
        )
        {
            return Result.Failure<UserResult>(DomainException.UserNotFound);
        }
        if (
            request.Email is not null
            && await userRepository.GetByEmailAsync(request.Email).ConfigureAwait(false) is not null
        )
        {
            return Result.Failure<UserResult>(DomainException.EmailAlreadyExists);
        }

        user.Update(
            request.Password,
            request.Email,
            request.Phone,
            request.FullName,
            request.Address,
            request.Otp
        );

        userRepository.Update(user);
        await unitOfWork.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

        return mapper.Map<UserResult>(user);
    }
}
