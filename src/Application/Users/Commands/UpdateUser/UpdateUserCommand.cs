namespace Application.Users.Commands.UpdateUser;

public record UpdateUserCommand(
    Guid Id,
    string? Password,
    string? Email,
    string? Phone,
    string? FullName,
    string? Address,
    string? Otp) : ICommand<UserResult>;
