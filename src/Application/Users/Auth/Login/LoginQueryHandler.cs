using Domain.Common.Shares;

namespace Application.Users.Auth.Login;

internal sealed class LoginQueryHandler(
        IJwtTokenService jwtTokenService,
        IUserRepository userRepository,
        IMapper mapper)
    : IQueryHandler<LoginQuery, AuthenticationResult>
{
    public async Task<Result<AuthenticationResult>> Handle(LoginQuery request, CancellationToken cancellationToken)
    {
        var user = await userRepository.GetByEmailAsync(request.UsernameOrEmail).ConfigureAwait(false);
        if (user is null)
        {
            user = await userRepository.GetByUsernameAsync(request.UsernameOrEmail).ConfigureAwait(false);
            if (user is null)
            {
                return Result.Failure<AuthenticationResult>(DomainException.UserNotFound);
            }
        }
        if (!user.VerifyPassword(request.Password))
        {
            return Result.Failure<AuthenticationResult>(DomainException.WrongPassword);
        }

        string token = jwtTokenService.GenerateToken(user);

        return new AuthenticationResult(mapper.Map<UserResult>(user), token);
    }
}
