using FluentValidation;

namespace Application.Users.Auth.Login;

public class LoginQueryValidator : AbstractValidator<LoginQuery>
{
    public LoginQueryValidator()
    {
        RuleFor(x => x.UsernameOrEmail)
            .NotEmpty()
            // .EmailAddress()
            .WithMessage(DomainException.InvalidEmail.Message);
        RuleFor(x => x.Password)
            .NotEmpty()
            .WithMessage(DomainException.InvalidPassword.Message);
    }
}
