namespace Application.Users.Auth.Login;

public record LoginQuery(
    string UsernameOrEmail,
    string Password) : IQuery<AuthenticationResult>;
