namespace Application.Users.Auth;

public record AuthenticationResult(
    UserResult User,
    string Token);

