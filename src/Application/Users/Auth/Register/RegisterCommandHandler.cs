using System.Security.Cryptography;
using Application.Abstractions.Persistence;
using Domain.Common.Shares;
using Domain.Users;

namespace Application.Users.Auth.Register;

internal sealed class RegisterCommandHandler(
        IJwtTokenService jwtTokenService,
        IUserRepository userRepository,
        IUnitOfWork unitOfWork,
        IMapper mapper)
    : ICommandHandler<RegisterCommand, AuthenticationResult>
{
    public async Task<Result<AuthenticationResult>> Handle(RegisterCommand request, CancellationToken cancellationToken)
    {
        if (await userRepository.GetByEmailAsync(request.Email).ConfigureAwait(false) is not null)
        {
            return Result.Failure<AuthenticationResult>(DomainException.EmailAlreadyExists);
        }
        if (await userRepository.GetByUsernameAsync(request.Username).ConfigureAwait(false) is not null)
        {
            return Result.Failure<AuthenticationResult>(DomainException.UsernameAlreadyExists);
        }

        using var hashAlgorithm = SHA256.Create();

        var user = User.Create(
            request.Username,
            request.Password,
            request.Email);

        string token = jwtTokenService.GenerateToken(user);
        userRepository.Add(user);
        await unitOfWork.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

        return new AuthenticationResult(mapper.Map<UserResult>(user), token);
    }
}
