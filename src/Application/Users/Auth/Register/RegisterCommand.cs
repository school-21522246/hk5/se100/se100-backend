namespace Application.Users.Auth.Register;

public record RegisterCommand(
    string Username,
    string Password,
    string Email) : ICommand<AuthenticationResult>;
