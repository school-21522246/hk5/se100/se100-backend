using FluentValidation;

namespace Application.Users.Auth.Register;

public class RegisterCommandValidator : AbstractValidator<RegisterCommand>
{
    public RegisterCommandValidator()
    {
        RuleFor(x => x.Username)
            .NotEmpty();
        RuleFor(x => x.Email)
            .NotEmpty()
            .EmailAddress()
            .WithMessage(DomainException.InvalidEmail.Message);
        RuleFor(x => x.Password)
            .NotEmpty()
            .WithMessage(DomainException.InvalidPassword.Message);
    }
}
