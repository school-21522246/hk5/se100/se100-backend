namespace Application.Users;

public record UserResult(
    Guid Id,
    string Username,
    string Email,
    string Phone,
    string FullName,
    string Address,
    string Otp,
    IList<RoleResult> Roles);

public record RoleResult(
    string Name,
    int Priority);
