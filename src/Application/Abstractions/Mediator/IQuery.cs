using Domain.Common.Shares;
using MediatR;

namespace Application.Abstractions.Mediator;
#pragma warning disable CA1040
// The code that's violating the rule is on this line.
public interface IQuery<TResponse> : IRequest<Result<TResponse>>
{
}
#pragma warning restore CA1040
