using Domain.Common.Shares;
using MediatR;

namespace Application.Abstractions.Mediator;
#pragma warning disable CA1040
public interface ICommand : IRequest<Result>
{
}

public interface ICommand<TResponse> : IRequest<Result<TResponse>>
{
}
#pragma warning restore ca1040
