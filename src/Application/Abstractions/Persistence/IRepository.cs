using Domain.Common.Primitives;
using Domain.Common.ValueObjects;

namespace Application.Abstractions.Persistence;

public interface IRepository<TAggregate, TId>
    where TAggregate : AggregateRoot<TId>
    where TId : BaseId
{
    Task<List<TAggregate>> GetAllAsync(CancellationToken cancellationToken = default);
    Task<TAggregate?> GetByIdAsync(TId id, CancellationToken cancellationToken = default);
    TAggregate Add(TAggregate aggregate);
    void Update(TAggregate aggregate);
    void Remove(TAggregate aggregate);
}
