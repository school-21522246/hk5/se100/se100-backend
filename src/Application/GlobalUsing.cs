global using AutoMapper;
global using Application.Abstractions.Mediator;
global using DomainException = Domain.Common.Exceptions.DomainException;
