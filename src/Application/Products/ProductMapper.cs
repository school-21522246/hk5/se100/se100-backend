using Domain.Products;
using Domain.Products.Entities;

namespace Application.Products;

public class ProductMapper : Profile
{
    public ProductMapper()
    {

        CreateMap<Catelogy, CatelogyResult>();

        CreateMap<ProductImage, ProductImageResult>();

        CreateMap<Product, ProductResult>()
            .ForMember(d => d.CatelogyId, opt => opt.MapFrom(s => s.Catelogy.Id.Value))
            .ForMember(d => d.Images, opt => opt.MapFrom(s => s.ProductImages));
    }
}

