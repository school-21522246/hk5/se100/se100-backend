using Application.Abstractions.Persistence;
using Domain.Common.Shares;
using Domain.Products.Entities;

namespace Application.Products.Commands.CreateCatelogy;

internal sealed class CreateCatelogyCommandHandler(
    IProductRepository productRepository,
    IUnitOfWork unitOfWork,
    IMapper mapper
) : ICommandHandler<CreateCatelogyCommand, CatelogyResult>
{
    public async Task<Result<CatelogyResult>> Handle(
        CreateCatelogyCommand request,
        CancellationToken cancellationToken
    )
    {
        var catelogy = Catelogy.Create(request.Name, request.Description, request.Image);

        productRepository.AddCatelogy(catelogy);
        await unitOfWork.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

        return mapper.Map<CatelogyResult>(catelogy);
    }
}
