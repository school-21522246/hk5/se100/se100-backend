namespace Application.Products.Commands.CreateCatelogy;

public record CreateCatelogyCommand(
    string Name,
    string Description,
    string Image) : ICommand<CatelogyResult>;
