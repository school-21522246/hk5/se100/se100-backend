using FluentValidation;

namespace Application.Products.Commands.CreateCatelogy;

public class CreateCatelogyCommandValidator : AbstractValidator<CreateCatelogyCommand>
{
    public CreateCatelogyCommandValidator()
    {
        RuleFor(x => x.Name)
            .MaximumLength(30)
            .NotEmpty();
        RuleFor(x => x.Description)
            .NotEmpty();
        RuleFor(x => x.Image)
            .NotEmpty();
    }
}
