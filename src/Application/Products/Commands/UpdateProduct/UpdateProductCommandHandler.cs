using Application.Abstractions.Persistence;
using Domain.Common.Shares;
using Domain.Products.Entities;
using Domain.Products.ValueObjects;

namespace Application.Products.Commands.UpdateProduct;

internal sealed class UpdateProductCommandHandler(
    IProductRepository productRepository,
    IUnitOfWork unitOfWork,
    IMapper mapper
) : ICommandHandler<UpdateProductCommand, ProductResult>
{
    public async Task<Result<ProductResult>> Handle(
        UpdateProductCommand request,
        CancellationToken cancellationToken
    )
    {
        if (
            await productRepository
                .GetByIdAsync(ProductId.Create(request.ProductId), cancellationToken)
                .ConfigureAwait(false)
            is not { } product
        )
        {
            return Result.Failure<ProductResult>(DomainException.ProductNotFound);
        }
        Catelogy? catelogy = null;
        if (request.CatelogyId.HasValue && request.CatelogyId != Guid.Empty)
        {
            if (
                await productRepository
                    .GetCatelogyByIdAsync(request.CatelogyId.Value, cancellationToken)
                    .ConfigureAwait(false)
                is not { } value
            )
            {
                return Result.Failure<ProductResult>(DomainException.CatelogyNotFound);
            }
            catelogy = value;
        }

        product.Update(
            request.Name,
            request.Description,
            request.Price,
            request.Quantity,
            catelogy
        );

        productRepository.Update(product);
        await unitOfWork.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

        return mapper.Map<ProductResult>(product);
    }
}
