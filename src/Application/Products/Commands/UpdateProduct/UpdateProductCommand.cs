namespace Application.Products.Commands.UpdateProduct;

public record UpdateProductCommand(
    Guid ProductId,
    string? Name,
    string? Description,
    decimal? Price,
    int? Quantity,
    Guid? CatelogyId) : ICommand<ProductResult>;
