using FluentValidation;

namespace Application.Products.Commands.UpdateProduct;

public class UpdateProductCommandValidator : AbstractValidator<UpdateProductCommand>
{
    public UpdateProductCommandValidator()
    {
        RuleFor(x => x.ProductId)
            .NotNull();
        RuleFor(x => x.Name)
            .MaximumLength(30);
        RuleFor(x => x.Description)
            .MaximumLength(50);
        RuleFor(x => x.Price)
            .GreaterThan(0);
        RuleFor(x => x.Quantity)
            .GreaterThanOrEqualTo(0);
        RuleFor(x => x.CatelogyId);
    }
}
