using Application.Abstractions.Persistence;
using Domain.Common.Shares;

namespace Application.Products.Commands.UpdateCatelogy;

internal sealed class UpdateCatelogyCommandHandler(
    IProductRepository productRepository,
    IUnitOfWork unitOfWork,
    IMapper mapper
) : ICommandHandler<UpdateCatelogyCommand, CatelogyResult>
{
    public async Task<Result<CatelogyResult>> Handle(
        UpdateCatelogyCommand request,
        CancellationToken cancellationToken
    )
    {
        if (
            await productRepository
                .GetCatelogyByIdAsync(request.Id, cancellationToken)
                .ConfigureAwait(false)
            is not { } catelogy
        )
        {
            return Result.Failure<CatelogyResult>(DomainException.CatelogyNotFound);
        }

        catelogy.Update(request.Name, request.Description, request.Image);

        productRepository.UpdateCatelogy(catelogy);
        await unitOfWork.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

        return mapper.Map<CatelogyResult>(catelogy);
    }
}
