using FluentValidation;

namespace Application.Products.Commands.UpdateCatelogy;

public class UpdateCatelogyCommandValidator : AbstractValidator<UpdateCatelogyCommand>
{
    public UpdateCatelogyCommandValidator()
    {
        RuleFor(x => x.Name)
            .MaximumLength(30);
        RuleFor(x => x.Description);
        RuleFor(x => x.Image);
    }
}
