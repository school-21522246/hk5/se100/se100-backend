namespace Application.Products.Commands.UpdateCatelogy;

public record UpdateCatelogyCommand(
    Guid Id,
    string? Name,
    string? Description,
    string? Image) : ICommand<CatelogyResult>;
