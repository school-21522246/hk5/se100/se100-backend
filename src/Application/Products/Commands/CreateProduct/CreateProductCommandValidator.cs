using FluentValidation;

namespace Application.Products.Commands.CreateProduct;

public class CreateProductCommandValidator : AbstractValidator<CreateProductCommand>
{
    public CreateProductCommandValidator()
    {
        RuleFor(x => x.Name)
            .MaximumLength(30)
            .NotEmpty();
        RuleFor(x => x.Description)
            .NotEmpty();
        RuleFor(x => x.Price)
            .GreaterThan(0)
            .NotEmpty();
        RuleFor(x => x.Quantity)
            .GreaterThanOrEqualTo(0)
            .NotEmpty();
        RuleFor(x => x.CatelogyId)
            .NotEmpty();
    }
}
