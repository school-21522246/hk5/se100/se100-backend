using Application.Abstractions.Persistence;
using Domain.Common.Shares;
using Domain.Products;

namespace Application.Products.Commands.CreateProduct;

internal sealed class CreateProductCommandHandler(
    IProductRepository productRepository,
    IUnitOfWork unitOfWork,
    IMapper mapper
) : ICommandHandler<CreateProductCommand, ProductResult>
{
    public async Task<Result<ProductResult>> Handle(
        CreateProductCommand request,
        CancellationToken cancellationToken
    )
    {
        if (
            await productRepository
                .GetCatelogyByIdAsync(request.CatelogyId, cancellationToken)
                .ConfigureAwait(false)
            is not { } catelogy
        )
        {
            return Result.Failure<ProductResult>(DomainException.CatelogyNotFound);
        }

        var product = Product.Create(
            request.Name,
            request.Description,
            request.Price,
            request.Quantity,
            catelogy
        );

        productRepository.Add(product);
        await unitOfWork.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

        return mapper.Map<ProductResult>(product);
    }
}
