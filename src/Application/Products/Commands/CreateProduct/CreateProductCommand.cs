namespace Application.Products.Commands.CreateProduct;

public record CreateProductCommand(
    string Name,
    string Description,
    decimal Price,
    int Quantity,
    Guid CatelogyId) : ICommand<ProductResult>;
