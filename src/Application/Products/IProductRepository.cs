using Application.Abstractions.Persistence;
using Application.Products.Queries.FilterProducts;
using Domain.Products;
using Domain.Products.Entities;
using Domain.Products.ValueObjects;

namespace Application.Products;

public interface IProductRepository : IRepository<Product, ProductId>
{
    Task<Catelogy?> GetCatelogyByIdAsync(Guid id, CancellationToken cancellationToken = default);
    Catelogy AddCatelogy(Catelogy catelogy);
    Task<List<Catelogy>> GetAllCatelogiesAsync();
    Task<List<Product>> FilterProductsAsync(FilterProductsQuery query);
    void UpdateCatelogy(Catelogy catelogy);
}
