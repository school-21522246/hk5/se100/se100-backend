using Domain.Common.Shares;

namespace Application.Products.Queries.GetAllCatelogys;

internal class GetAllCatelogysQueryHandler(IProductRepository productRepository, IMapper mapper)
    : IQueryHandler<GetAllCatelogysQuery, List<CatelogyResult>>
{
    public async Task<Result<List<CatelogyResult>>> Handle(
        GetAllCatelogysQuery request,
        CancellationToken cancellationToken
    )
    {
        var catelogys = await productRepository.GetAllCatelogiesAsync().ConfigureAwait(false);
        return catelogys.Select(mapper.Map<CatelogyResult>).ToList();
    }
}
