namespace Application.Products.Queries.FilterProducts;

public record FilterProductsQuery(
    string? Name,
    string? Description,
    decimal? MaxPrice,
    int? MinQuantity,
    Guid? CatelogyId) : IQuery<List<ProductResult>>;
