using FluentValidation;

namespace Application.Products.Queries.FilterProducts;

public class FilterProductsQueryValidator : AbstractValidator<FilterProductsQuery>
{
    public FilterProductsQueryValidator()
    {
        RuleFor(x => x.Name)
            .MaximumLength(30);
        RuleFor(x => x.Description)
            .MaximumLength(50);
        RuleFor(x => x.MaxPrice)
            .GreaterThan(0);
        RuleFor(x => x.MinQuantity)
            .GreaterThanOrEqualTo(0);
        RuleFor(x => x.CatelogyId);
    }
}
