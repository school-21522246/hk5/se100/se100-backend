using Domain.Common.Shares;

namespace Application.Products.Queries.FilterProducts;

internal class FilterProductsQueryHandler(IProductRepository productRepository, IMapper mapper)
    : IQueryHandler<FilterProductsQuery, List<ProductResult>>
{
    public async Task<Result<List<ProductResult>>> Handle(
        FilterProductsQuery request,
        CancellationToken cancellationToken
    )
    {
        var filteredProducts = await productRepository
            .FilterProductsAsync(request)
            .ConfigureAwait(false);
        return filteredProducts.Select(mapper.Map<ProductResult>).ToList();
    }
}
