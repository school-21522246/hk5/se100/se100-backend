namespace Application.Products.Queries.GetAllProducts;

public record GetAllProductsQuery() : IQuery<List<ProductResult>>;
