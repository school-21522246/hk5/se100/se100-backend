using Domain.Common.Shares;

namespace Application.Products.Queries.GetAllProducts;

internal class GetAllProductsQueryHandler(IProductRepository productRepository, IMapper mapper)
    : IQueryHandler<GetAllProductsQuery, List<ProductResult>>
{
    public async Task<Result<List<ProductResult>>> Handle(
        GetAllProductsQuery request,
        CancellationToken cancellationToken
    )
    {
        var products = await productRepository.GetAllAsync().ConfigureAwait(false);
        return products.Select(mapper.Map<ProductResult>).ToList();
    }
}
