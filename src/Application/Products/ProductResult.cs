namespace Application.Products;

public class ProductResult
{
    public Guid Id { get; init; }
    public string Name { get; init; } = string.Empty;
    public string Description { get; init; } = string.Empty;
    public decimal Price { get; init; }
    public int Quantity { get; init; }
    public Guid CatelogyId { get; init; }
    public IList<ProductImageResult> Images { get; init; } = new List<ProductImageResult>();
}

public class CatelogyResult
{
    public Guid Id { get; init; }
    public string Name { get; init; } = string.Empty;
    public string Description { get; init; } = string.Empty;
    public string Image { get; init; } = string.Empty;
}

public class ProductImageResult
{
    public Guid Id { get; init; }
    public string Image { get; init; } = string.Empty;
    public bool IsPrimary { get; init; }
}

