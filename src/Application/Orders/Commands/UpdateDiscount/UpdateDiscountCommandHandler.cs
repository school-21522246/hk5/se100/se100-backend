using Application.Abstractions.Persistence;
using Domain.Common.Shares;

namespace Application.Orders.Commands.UpdateDiscount;

internal sealed class UpdateDiscountCommandHandler(
    IOrderRepository orderRepository,
    IUnitOfWork unitOfWork,
    IMapper mapper
) : ICommandHandler<UpdateDiscountCommand, DiscountResult>
{
    public async Task<Result<DiscountResult>> Handle(
        UpdateDiscountCommand request,
        CancellationToken cancellationToken
    )
    {
        if (await orderRepository
                .GetDiscountByIdAsync(request.DiscountId, cancellationToken)
                .ConfigureAwait(false) is not { } discount)
        {
            return Result.Failure<DiscountResult>(DomainException.DiscountNotFound);
        }

        discount.Update(
            request.Description,
            request.Amount,
            request.StartDate,
            request.EndDate
        );

        await unitOfWork.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

        return mapper.Map<DiscountResult>(discount);
    }
}
