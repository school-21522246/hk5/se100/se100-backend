namespace Application.Orders.Commands.UpdateDiscount;

public record UpdateDiscountCommand(
    Guid DiscountId,
    string? Description,
    int? Amount,
    DateTime? StartDate,
    DateTime? EndDate) : ICommand<DiscountResult>;
