using FluentValidation;

namespace Application.Orders.Commands.UpdateDiscount;

public class UpdateDiscountCommandValidator : AbstractValidator<UpdateDiscountCommand>
{
    public UpdateDiscountCommandValidator()
    {
        RuleFor(x => x.DiscountId)
            .NotNull();
        RuleFor(x => x.Description);
        RuleFor(x => x.Amount)
            .GreaterThan(0);
        RuleFor(x => x.StartDate);
        RuleFor(x => x.EndDate);
    }
}
