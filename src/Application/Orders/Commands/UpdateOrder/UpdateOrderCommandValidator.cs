using FluentValidation;

namespace Application.Orders.Commands.UpdateOrder;

internal class UpdateOrderCommandValidator : AbstractValidator<UpdateOrderCommand>
{
    public UpdateOrderCommandValidator()
    {
        RuleFor(x => x.OrderId)
            .NotEmpty();
        RuleFor(x => x.ShipDate);
        RuleFor(x => x.Status);
    }
}

internal class UpdatePaymentCommandValidator : AbstractValidator<UpdatePaymentCommand>
{
    public UpdatePaymentCommandValidator()
    {
        RuleFor(x => x.PayDate);
        RuleFor(x => x.Status);
    }
}

