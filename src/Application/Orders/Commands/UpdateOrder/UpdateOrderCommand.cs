namespace Application.Orders.Commands.UpdateOrder;

public record UpdateOrderCommand(
    Guid OrderId,
    DateTime? ShipDate,
    string? Status,
    UpdatePaymentCommand? Payment
    ) : ICommand<OrderResult>;

public record UpdatePaymentCommand(
    DateTime? PayDate,
    string? Status
    // int Amount,
    // string PayType,
    ) : ICommand<OrderResult>;

