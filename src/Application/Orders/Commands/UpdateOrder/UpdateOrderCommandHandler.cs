using Application.Abstractions.Persistence;
using Domain.Common.Shares;
using Domain.Orders.ValueObjects;

namespace Application.Orders.Commands.UpdateOrder;

internal sealed class UpdateOrderCommandHandler(
    IOrderRepository orderRepository,
    IUnitOfWork unitOfWork,
    IMapper mapper
) : ICommandHandler<UpdateOrderCommand, OrderResult>
{
    public async Task<Result<OrderResult>> Handle(
        UpdateOrderCommand request,
        CancellationToken cancellationToken
    )
    {
        if (
            await orderRepository
                .GetByIdAsync(OrderId.Create(request.OrderId), cancellationToken)
                .ConfigureAwait(false)
            is not { } order
        )
        {
            return Result.Failure<OrderResult>(DomainException.OrderNotFound);
        }

        order.Payment.Update(
            request.Payment?.PayDate,
            request.Payment?.Status
        );
        order.Update(
            request.ShipDate,
            request.Status
        );

        await unitOfWork.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

        return mapper.Map<OrderResult>(order);
    }
}
