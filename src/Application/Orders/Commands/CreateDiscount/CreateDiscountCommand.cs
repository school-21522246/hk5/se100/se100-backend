namespace Application.Orders.Commands.CreateDiscount;

public record CreateDiscountCommand(
    string Description,
    int Amount,
    DateTime StartDate,
    DateTime EndDate) : ICommand<DiscountResult>;
