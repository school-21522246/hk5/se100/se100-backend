using FluentValidation;

namespace Application.Orders.Commands.CreateDiscount;

public class CreateDiscountCommandValidator : AbstractValidator<CreateDiscountCommand>
{
    public CreateDiscountCommandValidator()
    {
        RuleFor(x => x.Description)
            .NotEmpty();
        RuleFor(x => x.Amount)
            .GreaterThan(0)
            .NotEmpty();
        RuleFor(x => x.StartDate)
            .NotEmpty();
        RuleFor(x => x.EndDate)
            .NotEmpty();
    }
}
