using Application.Abstractions.Persistence;
using Domain.Common.Shares;
using Domain.Orders.Entities;

namespace Application.Orders.Commands.CreateDiscount;

internal sealed class CreateDiscountCommandHandler(
    IOrderRepository orderRepository,
    IUnitOfWork unitOfWork,
    IMapper mapper
) : ICommandHandler<CreateDiscountCommand, DiscountResult>
{
    public async Task<Result<DiscountResult>> Handle(
        CreateDiscountCommand request,
        CancellationToken cancellationToken
    )
    {
        var discount = Discount.Create(
            request.Description,
            request.Amount,
            request.StartDate,
            request.EndDate
        );

        orderRepository.AddDiscount(discount);
        await unitOfWork.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

        return mapper.Map<DiscountResult>(discount);
    }
}
