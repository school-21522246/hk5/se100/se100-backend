namespace Application.Orders.Commands.CreateOrder;

public record CreateOrderCommand(
    Guid UserId,
    DateTime OrderDate,
    DateTime ShipDate,
    string Status,
    string DeliveryAddress,
    CreatePaymentCommand Payment,
    IList<CreateOrderDetailCommand> OrderDetails) : ICommand<OrderResult>;

public record CreatePaymentCommand(
    DateTime PayDate,
    string Status,
    int Amount,
    string PayType) : ICommand<OrderResult>;

public record CreateOrderDetailCommand(
    int Quantity,
    decimal UnitPrice,
    Guid ProductId,
    Guid? DiscountId) : ICommand<OrderResult>;
