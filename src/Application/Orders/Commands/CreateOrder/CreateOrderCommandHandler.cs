using Application.Abstractions.Persistence;
using Application.Products;
using Application.Users;
using Domain.Common.Shares;
using Domain.Orders;
using Domain.Orders.Entities;
using Domain.Products.ValueObjects;
using Domain.Users.ValueObjects;

namespace Application.Orders.Commands.CreateOrder;

internal sealed class CreateOrderCommandHandler(
    IOrderRepository orderRepository,
    IUserRepository userRepository,
    IProductRepository productRepository,
    IUnitOfWork unitOfWork,
    IMapper mapper
) : ICommandHandler<CreateOrderCommand, OrderResult>
{
    public async Task<Result<OrderResult>> Handle(
        CreateOrderCommand request,
        CancellationToken cancellationToken
    )
    {
        if (
            await userRepository
                .GetByIdAsync(UserId.Create(request.UserId), cancellationToken)
                .ConfigureAwait(false)
            is not { } user
        )
        {
            return Result.Failure<OrderResult>(DomainException.UserNotFound);
        }

        var order = Order.Create(
            request.OrderDate,
            request.ShipDate,
            request.Status,
            request.DeliveryAddress,
            user,
            MapPayment(request.Payment)
        );

        foreach (var item in request.OrderDetails)
        {
            var result = await MapOrderDetails(item, cancellationToken).ConfigureAwait(false);
            if (result.IsFailure)
            {
                return Result.Failure<OrderResult>(result.Error);
            }
            order.AddOrderDetail(result.Value);
        }

        orderRepository.Add(order);
        await unitOfWork.SaveChangesAsync(cancellationToken).ConfigureAwait(false);

        return mapper.Map<OrderResult>(order);
    }

    private static Payment MapPayment(CreatePaymentCommand payment)
        => Payment.Create(
            payment.PayDate,
            payment.Status,
            payment.Amount,
            payment.PayType
        );

    private async Task<Result<OrderDetail>> MapOrderDetails(CreateOrderDetailCommand orderDetail, CancellationToken cancellationToken)
    {
        if (
            await productRepository
                .GetByIdAsync(ProductId.Create(orderDetail.ProductId), cancellationToken)
                .ConfigureAwait(false)
            is not { } product
        )
        {
            return Result.Failure<OrderDetail>(DomainException.ProductNotFound);
        }

        Discount? discount = null;
        if (orderDetail.DiscountId.HasValue && orderDetail.DiscountId != Guid.Empty)
        {
            if (
                await orderRepository
                    .GetDiscountByIdAsync(orderDetail.DiscountId.Value, cancellationToken)
                    .ConfigureAwait(false)
                is not { } value
            )
            {
                return Result.Failure<OrderDetail>(DomainException.DiscountNotFound);
            }

            discount = value;
        }

        return OrderDetail.Create(
            orderDetail.Quantity,
            orderDetail.UnitPrice,
            product,
            discount
        );
    }
}
