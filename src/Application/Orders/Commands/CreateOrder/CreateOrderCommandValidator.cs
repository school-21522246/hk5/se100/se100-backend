using FluentValidation;

namespace Application.Orders.Commands.CreateOrder;

internal class CreateOrderCommandValidator : AbstractValidator<CreateOrderCommand>
{
    public CreateOrderCommandValidator()
    {
        RuleFor(x => x.OrderDate)
            .NotEmpty();
        RuleFor(x => x.ShipDate)
            .NotEmpty();
        RuleFor(x => x.Status)
            .NotEmpty();
        RuleFor(x => x.DeliveryAddress)
            .NotEmpty();

        CreatePaymentCommandValidator();
        CreateOrderDetailCommandValidator();
    }

    private void CreatePaymentCommandValidator()
    {
        RuleFor(x => x.Payment.PayDate)
            .NotEmpty();
        RuleFor(x => x.Payment.Status)
            .NotEmpty();
        RuleFor(x => x.Payment.Amount)
            .GreaterThan(0)
            .NotEmpty();
        RuleFor(x => x.Payment.PayType)
            .NotEmpty();
    }

    private void CreateOrderDetailCommandValidator()
        => RuleForEach(x => x.OrderDetails)
            .ChildRules(orderDetail =>
            {
                orderDetail.RuleFor(x => x.Quantity)
                    .GreaterThan(0)
                    .NotEmpty();
                orderDetail.RuleFor(x => x.UnitPrice)
                    .GreaterThan(0)
                    .NotEmpty();
                orderDetail.RuleFor(x => x.ProductId)
                    .NotEmpty();
                orderDetail.RuleFor(x => x.DiscountId);
            });
}
