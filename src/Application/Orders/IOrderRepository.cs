using Application.Abstractions.Persistence;
using Application.Orders.Queries.FilterDiscounts;
using Application.Orders.Queries.FilterOrders;
using Domain.Orders;
using Domain.Orders.Entities;
using Domain.Orders.ValueObjects;

namespace Application.Orders;

public interface IOrderRepository : IRepository<Order, OrderId>
{
    void AddDiscount(Discount discount);
    Task<Discount?> GetDiscountByIdAsync(Guid id, CancellationToken cancellationToken);
    Task<List<Discount>> GetAllDiscountsAsync(CancellationToken cancellationToken);
    void UpdateDiscount(Discount discount);
    Task<List<Discount>> FilterDiscountsAsync(FilterDiscountsQuery query, CancellationToken cancellationToken = default);
    Task<List<Order>> FilterOrdersAsync(FilterOrdersQuery query, CancellationToken cancellationToken);
}
