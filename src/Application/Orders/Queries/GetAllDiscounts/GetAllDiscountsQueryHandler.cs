using Domain.Common.Shares;

namespace Application.Orders.Queries.GetAllDiscounts;

internal class GetAllDiscountsQueryHandler(IOrderRepository orderRepository, IMapper mapper)
    : IQueryHandler<GetAllDiscountsQuery, List<DiscountResult>>
{
    public async Task<Result<List<DiscountResult>>> Handle(
        GetAllDiscountsQuery request,
        CancellationToken cancellationToken
    )
    {
        var discounts = await orderRepository.GetAllDiscountsAsync(cancellationToken).ConfigureAwait(false);
        return discounts.Select(mapper.Map<DiscountResult>).ToList();
    }
}
