namespace Application.Orders.Queries.GetAllDiscounts;

public record GetAllDiscountsQuery() : IQuery<List<DiscountResult>>;
