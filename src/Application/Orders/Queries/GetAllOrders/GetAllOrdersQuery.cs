namespace Application.Orders.Queries.GetAllOrders;

public record GetAllOrdersQuery() : IQuery<List<OrderResult>>;
