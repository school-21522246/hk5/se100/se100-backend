using Domain.Common.Shares;

namespace Application.Orders.Queries.GetAllOrders;

internal class GetAllOrdersQueryHandler(IOrderRepository orderRepository, IMapper mapper)
    : IQueryHandler<GetAllOrdersQuery, List<OrderResult>>
{
    public async Task<Result<List<OrderResult>>> Handle(
        GetAllOrdersQuery request,
        CancellationToken cancellationToken
    )
    {
        var orders = await orderRepository.GetAllAsync(cancellationToken).ConfigureAwait(false);
        return orders.Select(mapper.Map<OrderResult>).ToList();
    }
}
