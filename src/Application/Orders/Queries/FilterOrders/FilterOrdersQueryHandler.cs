using Domain.Common.Shares;

namespace Application.Orders.Queries.FilterOrders;

internal class FilterOrdersQueryHandler(IOrderRepository orderRepository, IMapper mapper)
    : IQueryHandler<FilterOrdersQuery, List<OrderResult>>
{
    public async Task<Result<List<OrderResult>>> Handle(
        FilterOrdersQuery request,
        CancellationToken cancellationToken
    )
    {
        var filteredOrders = await orderRepository
            .FilterOrdersAsync(request, cancellationToken)
            .ConfigureAwait(false);
        return filteredOrders.Select(mapper.Map<OrderResult>).ToList();
    }
}
