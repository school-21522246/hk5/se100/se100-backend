using FluentValidation;

namespace Application.Orders.Queries.FilterOrders;

public class FilterOrdersQueryValidator : AbstractValidator<FilterOrdersQuery>
{
    public FilterOrdersQueryValidator()
    {
        RuleFor(x => x.UserId);
        RuleFor(x => x.FromOrderDate);
        RuleFor(x => x.ToOrderDate);
        RuleFor(x => x.FromShipDate);
        RuleFor(x => x.ToShipDate);
        RuleFor(x => x.MinAmount)
            .GreaterThan(0);
        RuleFor(x => x.MaxAmount)
            .GreaterThan(0);
        RuleFor(x => x.MinQuantity)
            .GreaterThan(0);
        RuleFor(x => x.MaxQuantity)
            .GreaterThan(0);
        RuleFor(x => x.Status);
        RuleFor(x => x.PaymentStatus);
        RuleFor(x => x.PaymentType);
    }
}
