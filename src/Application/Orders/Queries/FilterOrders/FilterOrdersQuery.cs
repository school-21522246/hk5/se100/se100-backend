namespace Application.Orders.Queries.FilterOrders;

public record FilterOrdersQuery(
    Guid? UserId,
    DateTime? FromOrderDate,
    DateTime? ToOrderDate,
    DateTime? FromShipDate,
    DateTime? ToShipDate,
    decimal? MinAmount,
    decimal? MaxAmount,
    int? MinQuantity,
    int? MaxQuantity,
    string? Status,
    string? PaymentStatus,
    string? PaymentType
) : IQuery<List<OrderResult>>;
