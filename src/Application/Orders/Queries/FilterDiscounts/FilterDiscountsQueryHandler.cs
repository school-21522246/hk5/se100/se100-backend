using Domain.Common.Shares;

namespace Application.Orders.Queries.FilterDiscounts;

internal class FilterDiscountsQueryHandler(IOrderRepository orderRepository, IMapper mapper)
    : IQueryHandler<FilterDiscountsQuery, List<DiscountResult>>
{
    public async Task<Result<List<DiscountResult>>> Handle(
        FilterDiscountsQuery request,
        CancellationToken cancellationToken
    )
    {
        var filteredDiscounts = await orderRepository
            .FilterDiscountsAsync(request, cancellationToken)
            .ConfigureAwait(false);
        return filteredDiscounts.Select(mapper.Map<DiscountResult>).ToList();
    }
}
