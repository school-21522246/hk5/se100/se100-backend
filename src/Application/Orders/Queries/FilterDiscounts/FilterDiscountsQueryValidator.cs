using FluentValidation;

namespace Application.Orders.Queries.FilterDiscounts;

public class FilterDiscountsQueryValidator : AbstractValidator<FilterDiscountsQuery>
{
    public FilterDiscountsQueryValidator()
    {
        RuleFor(x => x.MinAmount)
            .GreaterThan(0);
        RuleFor(x => x.MaxAmount)
            .GreaterThan(0);
        RuleFor(x => x.CheckAvailable)
            .NotNull();
    }
}
