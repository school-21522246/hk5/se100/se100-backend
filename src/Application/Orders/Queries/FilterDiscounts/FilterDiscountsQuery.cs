namespace Application.Orders.Queries.FilterDiscounts;

public record FilterDiscountsQuery(
    int? MinAmount,
    int? MaxAmount,
    bool CheckAvailable) : IQuery<List<DiscountResult>>;
