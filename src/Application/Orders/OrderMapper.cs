using Domain.Orders;
using Domain.Orders.Entities;

namespace Application.Orders;

public class OrderMapper : Profile
{
    public OrderMapper()
    {
        CreateMap<Payment, PaymentResult>();
        CreateMap<Discount, DiscountResult>();

        CreateMap<OrderDetail, OrderDetailResult>()
            .ForMember(d => d.ProductId, opt => opt.MapFrom(s => s.Product.Id));

        CreateMap<Order, OrderResult>()
            .ForMember(d => d.UserId, opt => opt.MapFrom(s => s.User.Id));
    }
}

