namespace Application.Orders;

public record OrderResult(
    Guid Id,
    DateTime ShipDate,
    string Status,
    decimal TotalAmount,
    string DeliveryAddress,

    Guid UserId,
    PaymentResult Payment,
    IList<OrderDetailResult> OrderDetails
);

public record OrderDetailResult(
    Guid Id,
    int Quantity,
    decimal UnitPrice,
    Guid ProductId,
    DiscountResult Discount
);

public record PaymentResult(
    Guid Id,
    DateTime PayDate,
    int Amount,
    string Status,
    string PayType
);

public record DiscountResult(
    Guid Id,
    string Description,
    int Amount,
    DateTime StartDate,
    DateTime EndDate
);
