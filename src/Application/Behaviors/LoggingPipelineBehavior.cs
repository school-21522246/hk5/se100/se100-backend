using MediatR;
using Microsoft.Extensions.Logging;

namespace Application.Behaviors;

public class LoggingPipelineBehavior<TRequest, TResponse>(ILogger<LoggingPipelineBehavior<TRequest, TResponse>> logger)
    : IPipelineBehavior<TRequest, TResponse>
    where TRequest : IRequest<TResponse>
    where TResponse : notnull
{
    public async Task<TResponse> Handle(
        TRequest request,
        RequestHandlerDelegate<TResponse> next,
        CancellationToken cancellationToken)
    {
        logger.LogInformation(
                "Starting request {@RequestName} {@DateTimeUtc}",
                typeof(TRequest).Name,
                DateTime.UtcNow);

        ArgumentNullException.ThrowIfNull(next, nameof(next));
        var result = await next().ConfigureAwait(false);

        // if (result is null)
        // {
        //     _logger.LogError(
        //             "Error in request {@RequestName} {@Error} {@DateTimeUtc}",
        //             typeof(TRequest).Name,
        //             null,
        //             DateTime.UtcNow);
        // }

        logger.LogInformation(
                "Completed request {@RequestName} {@DateTimeUtc}",
                typeof(TRequest).Name,
                DateTime.UtcNow);

        return result;
    }
}
