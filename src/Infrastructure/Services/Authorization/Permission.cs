using Domain.Users.Enums;

namespace Infrastructure.Services.Authorization;

public sealed class Permission
{
    public ICollection<Role> Roles { get; }

    private Permission(ICollection<Role> roles)
    {
        Roles = roles;
    }

    public static Permission AdminPermission => new(new[] { Role.Admin });
    public static Permission CustomerPermission => new(new[] { Role.Customer });
}
