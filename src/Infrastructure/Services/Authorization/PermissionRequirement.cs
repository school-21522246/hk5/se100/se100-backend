using Microsoft.AspNetCore.Authorization;
using Domain.Users.Enums;

namespace Infrastructure.Services.Authorization;

public class PermissionRequirement : IAuthorizationRequirement
{
    public Role Role { get; }

    private PermissionRequirement(Role role)
    {
        Role = role;
    }

    public static PermissionRequirement Admin => new(Role.Admin);
    public static PermissionRequirement Customer => new(Role.Customer);
}
