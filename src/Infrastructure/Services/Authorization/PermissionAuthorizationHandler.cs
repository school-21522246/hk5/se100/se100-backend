using Application.Users;
using Domain.Users.ValueObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.JsonWebTokens;

namespace Infrastructure.Services.Authorization;

public class PermissionAuthorizationHandler(IServiceScopeFactory serviceScopeFactory) : AuthorizationHandler<PermissionRequirement>
{
    protected override async Task HandleRequirementAsync(
            AuthorizationHandlerContext context, PermissionRequirement requirement)
    {
        string? userId = context?.User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Sub)?.Value;
        if (userId is null || !Guid.TryParse(userId, out var guid))
        {
            return;
        }

        using var scope = serviceScopeFactory.CreateScope();
        var userRepository = scope.ServiceProvider.GetRequiredService<IUserRepository>();
        var logger = scope.ServiceProvider.GetRequiredService<ILogger<PermissionAuthorizationHandler>>();

        if (await userRepository.GetByIdAsync(UserId.Create(guid)).ConfigureAwait(false) is not { } user)
        {
            logger.LogWarning("User with Id {UserId} was not found while trying to access an API with {Role} role.", userId, requirement?.Role);
            return;
        }

        foreach (var role in user.Roles)
        {
            if (role.Value <= requirement?.Role.Value)
            {
                context?.Succeed(requirement);
            }
        }
    }
}
