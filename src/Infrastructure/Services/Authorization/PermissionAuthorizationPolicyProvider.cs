using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;

namespace Infrastructure.Services.Authorization;

public class PermissionAuthorizationPolicyProvider(IOptions<AuthorizationOptions> options)
    : DefaultAuthorizationPolicyProvider(options)
{
    public override async Task<AuthorizationPolicy?> GetPolicyAsync(string policyName) =>
        await base.GetPolicyAsync(policyName).ConfigureAwait(false) is { } policy
            ? policy
            : new AuthorizationPolicyBuilder()
                .AddRequirements(
                    policyName switch
                    {
                        nameof(PermissionRequirement.Admin) => PermissionRequirement.Admin,
                        nameof(PermissionRequirement.Customer) => PermissionRequirement.Customer,
                        _
                            => throw new ArgumentOutOfRangeException(
                                nameof(policyName),
                                policyName,
                                null
                            )
                    }
                )
                .Build();
}
