using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Infrastructure.Services.Authentication;
using Infrastructure.Services.Authentication.Setup;
using Application.Abstractions.Commons;
using Infrastructure.Services.Commons;
using Infrastructure.Persistence.Repositories;
using Infrastructure.Persistence.EntityFramework;
using Application.Users.Auth;
using Application.Users;
using Application.Abstractions.Persistence;
using Application.Products;
using Infrastructure.Services.Authorization;
using Infrastructure.Persistence.SeedData;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.EntityFrameworkCore;
using Application.Orders;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services,
        ConfigurationManager configuration)
    {
        services
            .AddServices()
            .AddPersistence(configuration);

        return services;
    }

    private static IServiceCollection AddPersistence(this IServiceCollection services, ConfigurationManager configuration)
    {
        string? connectionString = configuration.GetConnectionString("DefaultConnection");
        services.AddDbContext<ApplicationDbContext>(options =>
        {
            options.UseSqlite(connectionString);
            // options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
        });

        services.AddScoped<IUnitOfWork>(sp => sp.GetRequiredService<ApplicationDbContext>());
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IProductRepository, ProductRepository>();
        services.AddScoped<IOrderRepository, OrderRepository>();
        return services;
    }

    private static IServiceCollection
        AddServices(this IServiceCollection services) => services
        .AddCommon()
        .AddAuth();

    private static IServiceCollection AddCommon(this IServiceCollection services)
    {
        services.AddSingleton<IDateTimeProvider, DateTimeProvider>();
        return services;
    }

    private static IServiceCollection AddAuth(this IServiceCollection services)
    {
        services
            .ConfigureOptions<JwtSettingsSetup>()
            .ConfigureOptions<JwtBearerOptionsSetup>()
            .AddSingleton<IJwtTokenService, JwtTokenService>();

        JsonWebTokenHandler.DefaultInboundClaimTypeMap.Clear();
        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer();

        services.AddSingleton<IAuthorizationHandler, PermissionAuthorizationHandler>();
        services.AddSingleton<IAuthorizationPolicyProvider, PermissionAuthorizationPolicyProvider>();

        return services;
    }

    public static async Task<WebApplication> UseSeedDataAsync(this WebApplication app)
    {
        ArgumentNullException.ThrowIfNull(app, nameof(app));
        using var scope = app.Services.CreateScope();
        var service = scope.ServiceProvider;

        var context = service.GetRequiredService<ApplicationDbContext>();
        await context.Database.MigrateAsync().ConfigureAwait(false);
        await SeedData.Initialize(context).ConfigureAwait(false);

        return app;
    }

}
