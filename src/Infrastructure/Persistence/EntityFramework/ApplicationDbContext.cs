using Application.Abstractions.Persistence;
using Domain.Orders;
using Domain.Orders.Entities;
using Domain.Products;
using Domain.Products.Entities;
using Domain.Users;
using Domain.Users.Enums;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.EntityFramework;

public class ApplicationDbContext(DbContextOptions options) : DbContext(options), IUnitOfWork
{
    // User AggregateRoot
    public DbSet<User> Users { get; set; } = null!;
    public DbSet<Role> Roles { get; set; } = null!;

    // Product AggregateRoot
    public DbSet<Product> Products { get; set; } = null!;
    public DbSet<ProductImage> ProductImages { get; set; } = null!;
    public DbSet<Catelogy> Catelogies { get; set; } = null!;

    // Order AggregateRoot
    public DbSet<Order> Orders { get; set; } = null!;
    public DbSet<Discount> Discounts { get; set; } = null!;
    // public DbSet<OrderDetail> OrderDetails { get; set; } = null!;
    // public DbSet<Payment> Payments { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        ArgumentNullException.ThrowIfNull(modelBuilder, nameof(modelBuilder));

        modelBuilder.ApplyConfigurationsFromAssembly(InfrastructureAssembly.Assembly);

        base.OnModelCreating(modelBuilder);
    }

    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default) =>
        base.SaveChangesAsync(cancellationToken);
}
