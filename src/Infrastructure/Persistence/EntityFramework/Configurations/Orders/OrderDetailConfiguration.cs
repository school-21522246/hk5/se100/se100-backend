using Domain.Orders.Entities;
using Domain.Orders.ValueObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.EntityFramework.Configurations.Orders;

internal class OrderDetailConfiguration : IEntityTypeConfiguration<OrderDetail>
{
    public void Configure(EntityTypeBuilder<OrderDetail> builder)
    {
        ArgumentNullException.ThrowIfNull(builder, nameof(builder));
        ConfigureOrderDetail(builder);
    }

    private static void ConfigureOrderDetail(EntityTypeBuilder<OrderDetail> builder)
    {
        builder.ToTable("Order_Detail");

        builder.HasKey(x => x.Id);
        builder
            .Property(x => x.Id)
            .ValueGeneratedNever()
            .HasConversion(id => id.Value, value => OrderDetailId.Create(value));
    }
}
