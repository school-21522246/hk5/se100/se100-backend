using Domain.Products.Entities;
using Domain.Products.ValueObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.EntityFramework.Configurations.Products;

internal class CatelogyConfiguration : IEntityTypeConfiguration<Catelogy>
{
    public void Configure(EntityTypeBuilder<Catelogy> builder)
    {
        ArgumentNullException.ThrowIfNull(builder, nameof(builder));
        ConfigureCatelogy(builder);
    }

    private static void ConfigureCatelogy(EntityTypeBuilder<Catelogy> builder)
    {
        builder.ToTable("Catelogy");

        builder.HasKey(x => x.Id);
        builder
            .Property(x => x.Id)
            .ValueGeneratedNever()
            .HasConversion(id => id.Value, value => CatelogyId.Create(value));
    }
}
