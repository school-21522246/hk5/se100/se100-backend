using Infrastructure.Persistence.EntityFramework;
using Infrastructure.Persistence.SeedData.Products;
using Infrastructure.Persistence.SeedData.Users;

namespace Infrastructure.Persistence.SeedData;

public static class SeedData
{
    public static string BasePath { get; } = "../Infrastructure/Persistence/SeedData/";


    public static async Task Initialize(ApplicationDbContext context)
    {
        await User.SeedUserAggregate(context).ConfigureAwait(false);
        await Product.SeedProductAggregate(context).ConfigureAwait(false);
    }


}
