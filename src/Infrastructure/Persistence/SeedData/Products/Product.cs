using System.Text.Json;
using Infrastructure.Persistence.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.SeedData.Products;

internal class Product
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public decimal Price { get; set; }
    public int Quantity { get; set; }
    public Guid CatelogyId { get; set; }

    public static async Task SeedProductAggregate(ApplicationDbContext context)
    {
        await Catelogy.SeedData(context).ConfigureAwait(false);
        await SeedData(context).ConfigureAwait(false);
        await ProductImage.SeedData(context).ConfigureAwait(false);
    }

    private static async Task SeedData(ApplicationDbContext context)
    {
        ArgumentNullException.ThrowIfNull(context, nameof(context));

        if (await context.Products.AnyAsync().ConfigureAwait(false))
        {
            return;
        }


        string data = await File.ReadAllTextAsync(Persistence.SeedData.SeedData.BasePath + "Products/Products.json").ConfigureAwait(false);
        if (JsonSerializer.Deserialize<List<Product>>(data) is not { } products)
        {
            throw new InvalidOperationException("Cannot deserialize products");
        }
        foreach (var p in products)
        {
            var catelogyId = Domain.Products.ValueObjects.CatelogyId.Create(p.CatelogyId);
            if (await context.Catelogies.FirstOrDefaultAsync(x => x.Id.Equals(catelogyId)).ConfigureAwait(false) is not { } catelogy)
            {
                // string catelogies = string.Join(", ", context.Catelogies.Select(x => x.Id.Value));
                // throw new InvalidOperationException("CatelogyId not found: " + catelogies);
                throw new InvalidOperationException("CatelogyId not found: " + p.CatelogyId);
            }

            var product = Domain.Products.Product.Create(
                p.Name,
                p.Description,
                p.Price,
                p.Quantity,
                catelogy,
                id: p.Id);

            context.Add(product);
        }

        await context.SaveChangesAsync().ConfigureAwait(false);
    }
}
