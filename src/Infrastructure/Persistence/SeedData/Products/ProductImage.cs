using System.Text.Json;
using Infrastructure.Persistence.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.SeedData.Products;

internal class ProductImage
{
    public Guid Id { get; set; }
    public string Image { get; set; } = string.Empty;
    public bool IsPrimary { get; set; }
    public Guid ProductId { get; set; }

    public static async Task SeedData(ApplicationDbContext context)
    {
        ArgumentNullException.ThrowIfNull(context, nameof(context));

        if (await context.ProductImages.AnyAsync().ConfigureAwait(false))
        {
            return;
        }

        string data = await File.ReadAllTextAsync(Persistence.SeedData.SeedData.BasePath + "Products/ProductImages.json").ConfigureAwait(false);
        if (JsonSerializer.Deserialize<List<ProductImage>>(data) is not { } productImages)
        {
            throw new InvalidOperationException("Cannot deserialize products");
        }
        foreach (var pi in productImages)
        {
            var productId = Domain.Products.ValueObjects.ProductId.Create(pi.ProductId);
            if (await context.Products.FirstOrDefaultAsync(x => x.Id.Equals(productId)).ConfigureAwait(false) is not { } product)
            {
                throw new InvalidOperationException("ProductId not found");
            }

            var productImage = Domain.Products.Entities.ProductImage.Create(
                pi.Image,
                product,
                id: pi.Id);

            productImage.SetPrimary(pi.IsPrimary);

            context.Add(productImage);
        }

        await context.SaveChangesAsync().ConfigureAwait(false);
    }
}
