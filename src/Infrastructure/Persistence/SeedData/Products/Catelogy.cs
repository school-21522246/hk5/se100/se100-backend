using System.Text.Json;
using Infrastructure.Persistence.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.SeedData.Products;

internal class Catelogy
{
    public Guid Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Description { get; set; } = string.Empty;
    public string Image { get; set; } = string.Empty;

    public static async Task SeedData(ApplicationDbContext context)
    {
        ArgumentNullException.ThrowIfNull(context, nameof(context));

        if (await context.Catelogies.AnyAsync().ConfigureAwait(false))
        {
            return;
        }

        string data = await File.ReadAllTextAsync(Persistence.SeedData.SeedData.BasePath + "Products/Catelogies.json").ConfigureAwait(false);
        if (JsonSerializer.Deserialize<List<Catelogy>>(data) is not { } catelogies)
        {
            throw new InvalidOperationException("Cannot deserialize catelogies");
        }
        foreach (var u in catelogies)
        {
            var catelogy = Domain.Products.Entities.Catelogy.Create(
                u.Name,
                u.Description,
                u.Image,
                id: u.Id);
            context.Add(catelogy);
        }

        await context.SaveChangesAsync().ConfigureAwait(false);
    }
}

