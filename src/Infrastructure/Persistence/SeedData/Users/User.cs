using System.Text.Json;
using Infrastructure.Persistence.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.SeedData.Users;

internal class User
{
    public Guid Id { get; set; }
    public string Username { get; set; } = string.Empty;
    public string Password { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;
    public string Phone { get; set; } = string.Empty;
    public string FullName { get; set; } = string.Empty;
    public string Address { get; set; } = string.Empty;
    public IList<string> Roles { get; set; } = new List<string>();

    public static Task SeedUserAggregate(ApplicationDbContext context) => SeedUsers(context);

    public static async Task SeedUsers(ApplicationDbContext context)
    {
        ArgumentNullException.ThrowIfNull(context, nameof(context));

        if (await context.Users.AnyAsync().ConfigureAwait(false))
        {
            return;
        }

        string data = await File.ReadAllTextAsync(SeedData.BasePath + "/Users/Users.json").ConfigureAwait(false);
        if (JsonSerializer.Deserialize<List<User>>(data) is not { } users)
        {
            throw new InvalidOperationException("Cannot deserialize users");
        }
        foreach (var u in users)
        {
            var user = Domain.Users.User.Create(
                u.Username,
                u.Password,
                u.Email,
                u.Phone,
                u.FullName,
                u.Address,
                id: u.Id);

            foreach (string roleName in u.Roles)
            {
                if (context.Roles.FirstOrDefault(x =>
                    x.Name == roleName) is not { } role)
                {
                    throw new InvalidOperationException("Role not found");
                }
                user.Roles.Add(role);
            }

            context.Add(user);
        }

        await context.SaveChangesAsync().ConfigureAwait(false);
    }
}
