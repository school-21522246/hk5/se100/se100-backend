using Application.Orders;
using Application.Orders.Queries.FilterDiscounts;
using Application.Orders.Queries.FilterOrders;
using Domain.Orders;
using Domain.Orders.Entities;
using Domain.Orders.ValueObjects;
using Domain.Users.ValueObjects;
using Infrastructure.Persistence.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repositories;

public class OrderRepository(ApplicationDbContext dbContext) : Repository<Order, OrderId>(dbContext), IOrderRepository
{
    protected override IQueryable<Order> DbSet
        => dbContext.Orders
            .Include(x => x.OrderDetails)
            .ThenInclude(x => x.Product)
            .Include(x => x.OrderDetails)
            .ThenInclude(x => x.Discount)
            .Include(x => x.Payment)
            .Include(x => x.User);

    public void AddDiscount(Discount discount) => dbContext.Discounts.Add(discount);

    public Task<List<Discount>> GetAllDiscountsAsync(CancellationToken cancellationToken)
        => dbContext.Discounts.ToListAsync(cancellationToken);

    public async Task<List<Discount>> FilterDiscountsAsync(FilterDiscountsQuery query, CancellationToken cancellationToken = default)
        => await dbContext.Discounts
                .Where(x =>
                    (!query.MinAmount.HasValue || x.Amount >= query.MinAmount) &&
                    (!query.MaxAmount.HasValue || x.Amount <= query.MaxAmount) &&
                    (!query.CheckAvailable || x.EndDate >= DateTime.Now)
                ).ToListAsync(cancellationToken).ConfigureAwait(false);

    public async Task<Discount?> GetDiscountByIdAsync(Guid id, CancellationToken cancellationToken)
        => await dbContext.Discounts
                .FirstOrDefaultAsync(x => x.Id.Equals(DiscountId.Create(id)), cancellationToken).ConfigureAwait(false);

    public void UpdateDiscount(Discount discount)
        => dbContext.Discounts.Update(discount);

    public async Task<List<Order>> FilterOrdersAsync(FilterOrdersQuery query, CancellationToken cancellationToken)
    {
        ArgumentNullException.ThrowIfNull(query, nameof(query));
        return await DbSet
                    .Where(x =>
                        (!query.FromOrderDate.HasValue || x.OrderDate >= query.FromOrderDate) &&
                        (!query.ToOrderDate.HasValue || x.OrderDate <= query.ToOrderDate) &&
                        (!query.FromShipDate.HasValue || x.ShipDate >= query.FromShipDate) &&
                        (!query.ToShipDate.HasValue || x.ShipDate <= query.ToShipDate) &&
                        (!query.MinAmount.HasValue || x.TotalAmount >= query.MinAmount) &&
                        (!query.MaxAmount.HasValue || x.TotalAmount <= query.MaxAmount) &&
                        (!query.MinQuantity.HasValue || x.OrderDetails.Sum(x => x.Quantity) >= query.MinQuantity) &&
                        (!query.MaxQuantity.HasValue || x.OrderDetails.Sum(x => x.Quantity) <= query.MaxQuantity) &&
                        (query.Status == null || x.Status == query.Status) &&
                        (query.PaymentStatus == null || x.Payment.Status == query.PaymentStatus) &&
                        (query.PaymentType == null || x.Payment.PayType == query.PaymentType) &&
                        (!query.UserId.HasValue || x.User.Id.Equals(UserId.Create(query.UserId.Value)))
                    ).ToListAsync(cancellationToken).ConfigureAwait(false);
    }
}
