using Application.Users;
using Domain.Users;
using Domain.Users.Enums;
using Domain.Users.ValueObjects;
using Infrastructure.Persistence.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repositories;

public class UserRepository(ApplicationDbContext dbContext) : Repository<User, UserId>(dbContext), IUserRepository
{
    protected override IQueryable<User> DbSet
        => dbContext.Users
            .Include(x => x.Roles);

    public override User Add(User aggregate)
    {
        ArgumentNullException.ThrowIfNull(aggregate, nameof(aggregate));
        if (dbContext.Roles.FirstOrDefault(x => x.Equals(Role.Customer)) is not { } customerRole)
        {
            throw new InvalidOperationException("Customer role not found");
        }
        aggregate.Roles.Add(customerRole);
        dbContext.Users.Add(aggregate);
        return aggregate;
    }

    public Task<User?> GetByEmailAsync(string email) =>
        dbContext.Users.FirstOrDefaultAsync(x =>
            x.Email == email);

    public Task<User?> GetByUsernameAsync(string username) =>
        dbContext.Users.FirstOrDefaultAsync(x =>
            x.Username == username);

    public Task<Role?> GetRoleAsync(Role role) =>
        dbContext.Roles.FirstOrDefaultAsync(x =>
            x.Equals(role));
}
