using System.Globalization;
using Application.Products;
using Application.Products.Queries.FilterProducts;
using Domain.Products.Entities;
using Domain.Products.ValueObjects;
using Infrastructure.Persistence.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Product = Domain.Products.Product;

namespace Infrastructure.Persistence.Repositories;

public class ProductRepository(ApplicationDbContext dbContext) : Repository<Product, ProductId>(dbContext), IProductRepository
{
    protected override IQueryable<Product> DbSet
        => dbContext.Products
            .Include(x => x.Catelogy)
            .Include(x => x.ProductImages);

    public async Task<Catelogy?> GetCatelogyByIdAsync(Guid id, CancellationToken cancellationToken = default)
        => await dbContext.Catelogies.FirstOrDefaultAsync(x => x.Id.Equals(CatelogyId.Create(id)), cancellationToken).ConfigureAwait(false);

    public Catelogy AddCatelogy(Catelogy catelogy)
    {
        dbContext.Catelogies.Add(catelogy);
        return catelogy;
    }

    public Task<List<Catelogy>> GetAllCatelogiesAsync()
        => dbContext.Catelogies.ToListAsync();

    public async Task<List<Product>> FilterProductsAsync(FilterProductsQuery query)
    {
        var products = await dbContext.Products
                .Include(x => x.Catelogy)
                .Include(x => x.ProductImages)
                .Where(x =>
                    (!query.MaxPrice.HasValue || x.Price <= query.MaxPrice) &&
                    (!query.MinQuantity.HasValue || x.Quantity >= query.MinQuantity) &&
                    (!query.CatelogyId.HasValue || x.Catelogy.Id.Equals(CatelogyId.Create(query.CatelogyId.Value)))
                ).ToListAsync().ConfigureAwait(false);

        return products
                .Where(x =>
                    CheckContains(x.Name, query.Name ?? "") &&
                    CheckContains(x.Description, query.Description ?? "")
                ).ToList();
    }

    private static bool CheckContains(string value, string keyword)
        => ToLower(value).Contains(
                ToLower(keyword), StringComparison.CurrentCulture);

    private static string ToLower(string value)
        => value.ToLower(CultureInfo.CurrentCulture);

    public void UpdateCatelogy(Catelogy catelogy)
        => dbContext.Update(catelogy);
}
