using AutoMapper;
using Application.Users;
using Application.Users.Auth;
using Contracts.User;
using Contracts.User.Auth;

namespace Api.Commons;

public class ApiMapper : Profile
{
    public ApiMapper()
    {
        ConfigUsers();
    }

    private void ConfigUsers()
    {
        CreateMap<RoleResult, RoleResponse>();
        CreateMap<UserResult, UserResponse>();
        CreateMap<AuthenticationResult, AuthenticationResponse>()
            .ConstructUsing(x => new AuthenticationResponse(
                x.User.Id,
                x.User.Username,
                x.User.Email,
                x.Token));
    }

}
