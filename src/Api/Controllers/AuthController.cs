using Application.Users.Auth.Login;
using Application.Users.Auth.Register;
using Contracts.User.Auth;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Api.Commons;
using AutoMapper;

namespace Api.Controllers;

[AllowAnonymous]
[Route("api/[controller]")]
public class AuthController(ISender sender, IMapper mapper) : ApiController
{
    [HttpPost("login")]
    public IActionResult Login(LoginRequest request)
    {
        ArgumentNullException.ThrowIfNull(request, nameof(request));
        var result = sender.Send(new LoginQuery(request.UsernameOrEmail, request.Password)).Result;

        return result.IsFailure
            ? HandleFailure(result)
            : Ok(mapper.Map<AuthenticationResponse>(result.Value));
    }

    [HttpPost("register")]
    public IActionResult Register(RegisterRequest request)
    {
        ArgumentNullException.ThrowIfNull(request, nameof(request));
        var result = sender
            .Send(new RegisterCommand(
                    request.Username,
                    request.Password,
                    request.Email)).Result;

        return result.IsFailure
            ? HandleFailure(result)
            : Ok(mapper.Map<AuthenticationResponse>(result.Value));
    }
}
