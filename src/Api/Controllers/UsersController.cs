using Api.Commons;
using Application.Users.Queries.GetAllUsers;
using Application.Users.Commands.NewAdmin;
using Contracts.User;
using Contracts.User.Requests;
using Infrastructure.Services.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MediatR;
using AutoMapper;
using Application.Users.Commands.UpdateUser;

namespace Api.Controllers;

public class UsersController(ISender sender, IMapper mapper) : ApiController
{
    [HttpGet("get-all")]
    [AllowAnonymous]
    public IActionResult GetAllUsers()
    {
        var result = sender.Send(new GetAllUsersQuery()).Result;

        return result.IsFailure
            ? HandleFailure(result)
            : Ok(result.Value.Select(mapper.Map<UserResponse>));
    }

    [HttpPost("new-admin")]
    [Authorize(Policy = nameof(PermissionRequirement.Admin))]
    public IActionResult NewAdmin(NewAdminRequest request)
    {
        ArgumentNullException.ThrowIfNull(request, nameof(request));
        var result = sender.Send(new NewAdminCommand(
            request.Username,
            request.Password,
            request.Email,
            request.Phone,
            request.FullName,
            request.Address)).Result;

        return result.IsFailure
            ? HandleFailure(result)
            : Ok(mapper.Map<UserResponse>(result.Value));
    }

    [HttpGet("hello-customer")]
    [Authorize(Policy = nameof(PermissionRequirement.Customer))]
    public IActionResult HelloCustomer() => Ok("Hello Customer");

    [HttpGet("hello-admin")]
    [Authorize(Policy = nameof(PermissionRequirement.Admin))]
    public IActionResult HelloAdmin() => Ok("Hello Admin");

    [HttpPut("{id}")]
    [Authorize(Policy = nameof(PermissionRequirement.Admin))]
    public IActionResult Update(Guid id, [FromQuery] UpdateUserRequest request)
    {
        ArgumentNullException.ThrowIfNull(request);
        var result = sender
            .Send(
                new UpdateUserCommand(
                    id,
                    request.Password,
                    request.Email,
                    request.Phone,
                    request.FullName,
                    request.Address,
                    request.Otp
                )
            )
            .Result;

        return result.IsFailure ? HandleFailure(result) : Ok(result.Value);
    }
}
