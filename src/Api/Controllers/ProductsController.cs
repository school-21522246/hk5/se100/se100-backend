using Api.Commons;
using Application.Products.Commands.CreateProduct;
using Application.Products.Commands.UpdateProduct;
using Application.Products.Queries.FilterProducts;
using Application.Products.Queries.GetAllProducts;
using Infrastructure.Services.Authorization;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

public class ProductsController(ISender sender) : ApiController
{
    [HttpGet("get-all")]
    [AllowAnonymous]
    public IActionResult GetAllProducts()
    {
        var result = sender.Send(new GetAllProductsQuery()).Result;

        return result.IsFailure ? HandleFailure(result) : Ok(result.Value);
    }

    [HttpPost]
    [Authorize(Policy = nameof(PermissionRequirement.Admin))]
    public IActionResult Create(CreateProductCommand request)
    {
        var result = sender.Send(request).Result;

        return result.IsFailure ? HandleFailure(result) : Ok(result.Value);
    }

    [HttpPut]
    [Authorize(Policy = nameof(PermissionRequirement.Admin))]
    public IActionResult Update(UpdateProductCommand request)
    {
        ArgumentNullException.ThrowIfNull(request);
        var result = sender.Send(request).Result;

        return result.IsFailure ? HandleFailure(result) : Ok(result.Value);
    }

    [HttpGet("filter")]
    [AllowAnonymous]
    public IActionResult FilterProducts([FromQuery] FilterProductsQuery request)
    {
        var result = sender.Send(request).Result;

        return result.IsFailure ? HandleFailure(result) : Ok(result.Value);
    }
}
