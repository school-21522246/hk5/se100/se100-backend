using Api.Commons;
using Application.Products.Queries.GetAllCatelogys;
using Application.Products.Commands.CreateCatelogy;
using Infrastructure.Services.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using MediatR;
using Contracts.Products;
using Application.Products.Commands.UpdateCatelogy;

namespace Api.Controllers;

public class CatelogiesController(ISender sender) : ApiController
{
    [HttpGet("get-all")]
    [AllowAnonymous]
    public IActionResult GetAllCatelogys()
    {
        var result = sender.Send(new GetAllCatelogysQuery()).Result;

        return result.IsFailure
            ? HandleFailure(result)
            : Ok(result.Value);
    }

    [HttpPost]
    [Authorize(Policy = nameof(PermissionRequirement.Admin))]
    public IActionResult Create(CreateCatelogyCommand request)
    {
        var result = sender.Send(request).Result;

        return result.IsFailure
            ? HandleFailure(result)
            : Ok(result.Value);
    }

    [HttpPut("{id}")]
    [Authorize(Policy = nameof(PermissionRequirement.Admin))]
    public IActionResult Update(Guid id, [FromQuery] UpdateCatelogyRequest request)
    {
        ArgumentNullException.ThrowIfNull(request);
        var result = sender.Send(new UpdateCatelogyCommand(
                    id,
                    request.Name,
                    request.Description,
                    request.Image
                )).Result;

        return result.IsFailure
            ? HandleFailure(result)
            : Ok(result.Value);
    }

}
