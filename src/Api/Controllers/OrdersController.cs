using Api.Commons;
using Application.Orders.Commands.CreateDiscount;
using Application.Orders.Commands.CreateOrder;
using Application.Orders.Commands.UpdateDiscount;
using Application.Orders.Commands.UpdateOrder;
using Application.Orders.Queries.FilterDiscounts;
using Application.Orders.Queries.FilterOrders;
using Application.Orders.Queries.GetAllDiscounts;
using Application.Orders.Queries.GetAllOrders;
using Infrastructure.Services.Authorization;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

public class OrdersController(ISender sender) : ApiController
{
    [HttpGet("get-all")]
    [Authorize(Policy = nameof(PermissionRequirement.Admin))]
    public IActionResult GetAllOrders()
    {
        var result = sender.Send(new GetAllOrdersQuery()).Result;

        return result.IsFailure ? HandleFailure(result) : Ok(result.Value);
    }

    [HttpPost]
    public IActionResult Create(CreateOrderCommand request)
    {
        var result = sender.Send(request).Result;

        return result.IsFailure ? HandleFailure(result) : Ok(result.Value);
    }

    [HttpPut]
    public IActionResult Update(UpdateOrderCommand request)
    {
        ArgumentNullException.ThrowIfNull(request);
        var result = sender.Send(request).Result;

        return result.IsFailure ? HandleFailure(result) : Ok(result.Value);
    }

    [HttpGet("filter")]
    public IActionResult FilterOrders([FromQuery] FilterOrdersQuery request)
    {
        var result = sender.Send(request).Result;

        return result.IsFailure ? HandleFailure(result) : Ok(result.Value);
    }

    [HttpGet("/api/Discounts/get-all")]
    [Authorize(Policy = nameof(PermissionRequirement.Admin))]
    public IActionResult GetAllDiscounts()
    {
        var result = sender.Send(new GetAllDiscountsQuery()).Result;

        return result.IsFailure ? HandleFailure(result) : Ok(result.Value);
    }

    [HttpPost("/api/Discounts")]
    public IActionResult CreateDiscount(CreateDiscountCommand request)
    {
        var result = sender.Send(request).Result;

        return result.IsFailure ? HandleFailure(result) : Ok(result.Value);
    }

    [HttpPut("/api/Discounts")]
    public IActionResult UpdateDiscount(UpdateDiscountCommand request)
    {
        ArgumentNullException.ThrowIfNull(request);
        var result = sender.Send(request).Result;

        return result.IsFailure ? HandleFailure(result) : Ok(result.Value);
    }

    [HttpGet("/api/Discounts/filter")]
    [Authorize(Policy = nameof(PermissionRequirement.Admin))]
    public IActionResult FilterDiscounts([FromQuery] FilterDiscountsQuery request)
    {
        var result = sender.Send(request).Result;

        return result.IsFailure ? HandleFailure(result) : Ok(result.Value);
    }
}
