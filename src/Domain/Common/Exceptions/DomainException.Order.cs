using Domain.Common.Shares;

namespace Domain.Common.Exceptions;

public static partial class DomainException
{
    public static Error OrderNotFound => new("OrderNotFound", "Order not found.");
    public static Error DiscountNotFound => new("DiscountNotFound", "Discount not found.");
}
