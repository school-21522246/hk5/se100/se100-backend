using Domain.Common.Shares;

namespace Domain.Common.Exceptions;

public static partial class DomainException
{
    //Money
    public static Error InvalidCurrency => new("Invalid Currency", "this currency number doesn't exist");
    public static Error InvalidAmount => new("Invalid Amount", "this amount number doesn't exist");
    public static Error InvalidPhoneNumber => new("Invalid Phone Number", "Phone number only contains numbers and length is 10-11");
    public static Error NotEnoughMoney => new("Not Enough Money", "this amount number doesn't exist");
}
