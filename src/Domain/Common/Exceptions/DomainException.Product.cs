using Domain.Common.Shares;

namespace Domain.Common.Exceptions;

public static partial class DomainException
{
    public static Error ProductNotFound => new("ProductNotFound", "Product not found.");
    public static Error CatelogyNotFound => new("CatelogyNotFound", "Catelogy not found.");
}
