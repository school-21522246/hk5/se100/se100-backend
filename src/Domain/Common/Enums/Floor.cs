using Domain.Common.Primitives;

namespace Domain.Common.Enums;

public sealed class Floor : Enumeration<Floor>
{
    public static readonly Floor FloorB = new(-1, nameof(FloorB));
    public static readonly Floor FloorG = new(0, nameof(FloorG));
    public static readonly Floor Floor1 = new(1, nameof(Floor1));
    public static readonly Floor Floor2 = new(2, nameof(Floor2));

    private Floor(int value, string name) : base(value, name)
    {
    }
}
