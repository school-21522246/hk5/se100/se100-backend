using Domain.Common.Primitives;

namespace Domain.Common.ValueObjects;

public abstract class BaseId(Guid value) : ValueObject
{
    public Guid Value { get; } = value;

    public override IEnumerable<object> GetAtomicValues()
    {
        yield return Value;
    }

    public static Guid NewId => Guid.NewGuid();
}

