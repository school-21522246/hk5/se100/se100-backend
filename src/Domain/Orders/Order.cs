using Domain.Common.Primitives;
using Domain.Common.ValueObjects;
using Domain.Orders.Entities;
using Domain.Orders.ValueObjects;
using Domain.Users;

namespace Domain.Orders;

public class Order : AggregateRoot<OrderId>
{
    private Order(
        OrderId id,
        DateTime orderDate,
        DateTime shipDate,
        string status,
        string deliveryAddress,
        User user,
        Payment payment
    )
        : base(id)
    {
        OrderDate = orderDate;
        ShipDate = shipDate;
        Status = status;
        DeliveryAddress = deliveryAddress;
        User = user;
        Payment = payment;
    }

    public DateTime OrderDate { get; private set; }
    public DateTime ShipDate { get; private set; }
    public string Status { get; private set; }
    public decimal TotalAmount { get; private set; } = 0;
    public string DeliveryAddress { get; private set; }

    public User User { get; private set; }
    public Payment Payment { get; private set; }

    public IList<OrderDetail> OrderDetails { get; private set; } = new List<OrderDetail>();

    public static Order Create(
        DateTime orderDate,
        DateTime shipDate,
        string status,
        string deliveryAddress,
        User user,
        Payment payment,
        Guid? id = null
    )
        => new(
            OrderId.Create(id ?? BaseId.NewId),
            orderDate,
            shipDate,
            status,
            deliveryAddress,
            user,
            payment);

    public void Update(
        DateTime? shipDate,
        string? status
    )
    {
        ShipDate = shipDate ?? ShipDate;
        Status = status ?? Status;
    }

    public void AddOrderDetail(OrderDetail orderDetail)
    {
        ArgumentNullException.ThrowIfNull(orderDetail);
        OrderDetails.Add(orderDetail);
        TotalAmount += orderDetail.UnitPrice * orderDetail.Quantity;
    }

#pragma warning disable CS8618
    private Order() { }
#pragma warning restore CS8618
}
