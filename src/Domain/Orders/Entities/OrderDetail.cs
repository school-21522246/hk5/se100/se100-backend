using Domain.Common.Primitives;
using Domain.Common.ValueObjects;
using Domain.Orders.ValueObjects;
using Domain.Products;

namespace Domain.Orders.Entities;

public class OrderDetail : AggregateRoot<OrderDetailId>
{
    private OrderDetail(
        OrderDetailId id,
        int quantity,
        decimal unitPrice,
        Product product,
        Discount? discount)
        : base(id)
    {
        Quantity = quantity;
        UnitPrice = unitPrice;
        Product = product;
        Discount = discount;
    }

    public int Quantity { get; private set; }
    public decimal UnitPrice { get; private set; }

    public Product Product { get; private set; }
    public Discount? Discount { get; private set; }

    public static OrderDetail Create(
        int quantity,
        decimal unitPrice,
        Product product,
        Discount? discount = null,
        Guid? id = null
    ) => new(
            OrderDetailId.Create(id ?? BaseId.NewId),
            quantity,
            unitPrice,
            product,
            discount
        );

    public void Update(
        int? quantity,
        decimal? unitPrice
    )
    {
        Quantity = quantity ?? Quantity;
        UnitPrice = unitPrice ?? UnitPrice;
    }

#pragma warning disable CS8618
    private OrderDetail() { }
#pragma warning restore CS8618
}
