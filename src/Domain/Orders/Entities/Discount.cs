using Domain.Common.Primitives;
using Domain.Common.ValueObjects;
using Domain.Orders.ValueObjects;

namespace Domain.Orders.Entities;

public class Discount : AggregateRoot<DiscountId>
{
    private Discount(
        DiscountId id,
        string description,
        int amount,
        DateTime startDate,
        DateTime endDate
    )
        : base(id)
    {
        Description = description;
        Amount = amount;
        StartDate = startDate;
        EndDate = endDate;
    }

    public string Description { get; private set; }
    public int Amount { get; private set; }
    public DateTime StartDate { get; private set; }
    public DateTime EndDate { get; private set; }

    public static Discount Create(
        string description,
        int amount,
        DateTime startDate,
        DateTime endDate,
        Guid? id = null
    ) => new(
            DiscountId.Create(id ?? BaseId.NewId),
            description,
            amount,
            startDate,
            endDate
        );

    public void Update(
        string? description,
        int? amount,
        DateTime? startDate,
        DateTime? endDate
    )
    {
        Description = description ?? Description;
        Amount = amount ?? Amount;
        StartDate = startDate ?? StartDate;
        EndDate = endDate ?? EndDate;
    }

#pragma warning disable CS8618
    private Discount() { }
#pragma warning restore CS8618
}
