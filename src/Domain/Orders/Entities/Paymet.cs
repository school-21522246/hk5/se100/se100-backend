using Domain.Common.Primitives;
using Domain.Common.ValueObjects;
using Domain.Orders.ValueObjects;

namespace Domain.Orders.Entities;

public class Payment : AggregateRoot<PaymentId>
{
    private Payment(
        PaymentId id,
        DateTime payDate,
        string status,
        int amount,
        string payType
    )
        : base(id)
    {
        PayDate = payDate;
        Status = status;
        Amount = amount;
        PayType = payType;
    }

    public DateTime PayDate { get; private set; }
    public string Status { get; private set; }
    public int Amount { get; private set; }
    public string PayType { get; private set; }

    public static Payment Create(
        DateTime payDate,
        string status,
        int amount,
        string payType,
        Guid? id = null
    ) => new(
            PaymentId.Create(id ?? BaseId.NewId),
            payDate,
            status,
            amount,
            payType);

    public void Update(
        DateTime? payDate,
        // int? amount,
        // string? payType
        string? status
    )
    {
        PayDate = payDate ?? PayDate;
        Status = status ?? Status;
    }

#pragma warning disable CS8618
    private Payment() { }
#pragma warning restore CS8618
}
