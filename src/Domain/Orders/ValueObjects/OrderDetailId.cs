﻿using Domain.Common.ValueObjects;

namespace Domain.Orders.ValueObjects;

public class OrderDetailId : BaseId
{
    private OrderDetailId(Guid value) : base(value)
    {
    }

    public static OrderDetailId Create(Guid value) => new(value);
    public static new OrderDetailId NewId => new(BaseId.NewId);
}
