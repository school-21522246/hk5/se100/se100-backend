﻿using Domain.Common.ValueObjects;

namespace Domain.Orders.ValueObjects;

public class PaymentId : BaseId
{
    private PaymentId(Guid value) : base(value)
    {
    }

    public static PaymentId Create(Guid value) => new(value);
    public static new PaymentId NewId => new(BaseId.NewId);
}
