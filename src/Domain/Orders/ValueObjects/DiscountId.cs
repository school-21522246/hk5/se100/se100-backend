﻿using Domain.Common.ValueObjects;

namespace Domain.Orders.ValueObjects;

public class DiscountId : BaseId
{
    private DiscountId(Guid value) : base(value)
    {
    }

    public static DiscountId Create(Guid value) => new(value);
    public static new DiscountId NewId => new(BaseId.NewId);
}
