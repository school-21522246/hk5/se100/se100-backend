using System.Security.Cryptography;
using System.Text;
using Domain.Common.Primitives;
using Domain.Common.ValueObjects;
using Domain.Users.Enums;
using Domain.Users.ValueObjects;

namespace Domain.Users;

public class User : AggregateRoot<UserId>
{
    private User(
        UserId id,
        string username,
        byte[] passwordHash,
        byte[] passwordSalt,
        string email,
        string phone,
        string fullName,
        string address,
        string otp
    )
        : base(id)
    {
        Username = username;
        PasswordHash = passwordHash;
        PasswordSalt = passwordSalt;
        Email = email;
        Phone = phone;
        FullName = fullName;
        Address = address;
        Otp = otp;
    }

    public string Username { get; private set; }
    public IList<byte> PasswordHash { get; private set; }
    public IList<byte> PasswordSalt { get; private set; }
    public string Email { get; private set; }

    public IList<Role> Roles { get; private set; } = new List<Role>();

    public string Phone { get; private set; }
    public string FullName { get; private set; }
    public string Address { get; private set; }
    public string Otp { get; private set; }

    public bool VerifyPassword(string password)
    {
        using var hmac = new HMACSHA256([.. PasswordSalt]);
        byte[] computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
        return computedHash.SequenceEqual(PasswordHash);
    }

    public static User Create(
        string username,
        string password,
        string email,
        string phone = "",
        string fullName = "",
        string address = "",
        string otp = "",
        Guid? id = null
    )
    {
        using var hmac = new HMACSHA256();
        return new(
            UserId.Create(id ?? BaseId.NewId),
            username,
            hmac.ComputeHash(Encoding.UTF8.GetBytes(password)),
            hmac.Key,
            email,
            phone,
            fullName,
            address,
            otp
        );
    }


    public void Update(
        string? password,
        string? email,
        string? phone,
        string? fullName,
        string? address,
        string? otp
    )
    {
        if (password is not null)
        {
            using var hmac = new HMACSHA256();
            PasswordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            PasswordSalt = hmac.Key;
        }
        Email = email ?? Email;
        Phone = phone ?? Phone;
        FullName = fullName ?? FullName;
        Address = address ?? Address;
        Otp = otp ?? Otp;
    }


#pragma warning disable CS8618
    private User() { }
#pragma warning restore CS8618
}
