using Domain.Common.Primitives;

namespace Domain.Users.Enums;

public class Role : Enumeration<Role>
{
    public static readonly Role Admin = new(1, nameof(Admin));
    public static readonly Role Customer = new(2, nameof(Customer));

    private Role(int priority, string name) : base(priority, name)
    {
    }

#pragma warning disable CS8618
    private Role() { }
#pragma warning restore CS8618
}
