﻿using Domain.Common.ValueObjects;

namespace Domain.Products.ValueObjects;

public class CatelogyId : BaseId
{
    private CatelogyId(Guid value) : base(value)
    {
    }

    public static CatelogyId Create(Guid value) => new(value);
    public static new CatelogyId NewId => new(BaseId.NewId);
}
