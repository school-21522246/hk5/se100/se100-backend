﻿using Domain.Common.ValueObjects;

namespace Domain.Products.ValueObjects;

public class ProductImageId : BaseId
{
    private ProductImageId(Guid value) : base(value)
    {
    }

    public static ProductImageId Create(Guid value) => new(value);
    public static new ProductImageId NewId => new(BaseId.NewId);
}
