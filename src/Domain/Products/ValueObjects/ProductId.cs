﻿using Domain.Common.ValueObjects;

namespace Domain.Products.ValueObjects;

public class ProductId : BaseId
{
    private ProductId(Guid value) : base(value)
    {
    }

    public static ProductId Create(Guid value) => new(value);
    public static new ProductId NewId => new(BaseId.NewId);
}
