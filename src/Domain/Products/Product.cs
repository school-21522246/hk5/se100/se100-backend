using Domain.Common.Primitives;
using Domain.Common.ValueObjects;
using Domain.Products.Entities;
using Domain.Products.ValueObjects;

namespace Domain.Products;

public class Product : AggregateRoot<ProductId>
{
    private Product(
        ProductId id,
        string name,
        string description,
        decimal price,
        int quantity,
        Catelogy catelogy)
        : base(id)
    {
        Name = name;
        Description = description;
        Price = price;
        Quantity = quantity;
        Catelogy = catelogy;
    }

    public string Name { get; private set; }
    public string Description { get; private set; }
    public decimal Price { get; private set; }
    public int Quantity { get; private set; }
    public Catelogy Catelogy { get; private set; }

    public IList<ProductImage> ProductImages { get; private set; } = [];

    public static Product Create(
        string name,
        string description,
        decimal price,
        int quantity,
        Catelogy catelogy,
        Guid? id = null)
        => new(
            ProductId.Create(id ?? BaseId.NewId),
            name,
            description,
            price,
            quantity,
            catelogy);

    public void Update(
        string? name,
        string? description,
        decimal? price,
        int? quantity,
        Catelogy? catelogy)
    {
        Name = name ?? Name;
        Description = description ?? Description;
        Price = price ?? Price;
        Quantity = quantity ?? Quantity;
        Catelogy = catelogy ?? Catelogy;
    }

#pragma warning disable CS8618
    private Product() { }
#pragma warning restore CS8618
}
