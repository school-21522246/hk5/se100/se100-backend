using Domain.Common.Primitives;
using Domain.Common.ValueObjects;
using Domain.Products.ValueObjects;

namespace Domain.Products.Entities;

public class ProductImage : Entity<ProductImageId>
{
    private ProductImage(ProductImageId id, string image, bool isPrimary, Product product)
        : base(id)
    {
        Image = image;
        IsPrimary = isPrimary;
        Product = product;
    }

    public string Image { get; private set; }
    public bool IsPrimary { get; private set; }
    public Product Product { get; private set; }

    public static ProductImage Create(
        string image,
        Product product,
        Guid? id = null)
        => new(
            ProductImageId.Create(id ?? BaseId.NewId),
            image,
            false,
            product);

    public void SetPrimary(bool primary) => IsPrimary = primary;

#pragma warning disable CS8618
    private ProductImage() { }
#pragma warning restore CS8618
}

