using Domain.Common.Primitives;
using Domain.Common.ValueObjects;
using Domain.Products.ValueObjects;

namespace Domain.Products.Entities;

public class Catelogy : Entity<CatelogyId>
{
    public IList<Product> Products { get; private set; } = new List<Product>();

    private Catelogy(
        CatelogyId id,
        string name,
        string description,
        string image)
        : base(id)
    {
        Image = image;
        Name = name;
        Description = description;
    }

    public string Name { get; private set; }
    public string Description { get; private set; }
    public string Image { get; private set; }


    public static Catelogy Create(
        string name,
        string description,
        string image,
        Guid? id = null)
        => new(
            CatelogyId.Create(id ?? BaseId.NewId),
            name,
            description,
            image);

    public void Update(
        string? name,
        string? description,
        string? image)
    {
        Name = name ?? Name;
        Description = description ?? Description;
        Image = image ?? Image;
    }

#pragma warning disable CS8618
    private Catelogy() { }
#pragma warning restore CS8618
}
