namespace Contracts.User;

public record UserResponse(
    Guid Id,
    string Username,
    string Email,
    string Phone,
    string FullName,
    string Address,
    string Otp,
    IList<RoleResponse> Roles);

public record RoleResponse(
    string Name,
    int Priority);
