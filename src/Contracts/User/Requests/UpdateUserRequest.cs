namespace Contracts.User.Requests;

public record UpdateUserRequest(
    string? Password,
    string? Email,
    string? Phone,
    string? FullName,
    string? Address,
    string? Otp);

