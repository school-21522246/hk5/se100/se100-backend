namespace Contracts.User.Requests;

public record NewAdminRequest(
    string Username,
    string Password,
    string Email,
    string Phone,
    string FullName,
    string Address);

