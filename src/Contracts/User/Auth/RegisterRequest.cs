namespace Contracts.User.Auth;

public record RegisterRequest(
    string Username,
    string Email,
    string Phone,
    string Password);
