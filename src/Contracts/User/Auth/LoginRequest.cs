namespace Contracts.User.Auth;

public record LoginRequest(
    string UsernameOrEmail,
    string Password);
