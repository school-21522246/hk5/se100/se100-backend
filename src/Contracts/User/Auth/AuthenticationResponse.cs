namespace Contracts.User.Auth;

public record AuthenticationResponse(
    Guid Id,
    string Username,
    string Email,
    string Token);
