namespace Contracts.Products;

public record UpdateCatelogyRequest(
    string? Name,
    string? Description,
    string? Image);

