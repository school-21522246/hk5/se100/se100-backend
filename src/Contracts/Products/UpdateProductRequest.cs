namespace Contracts.Products;

public record UpdateProductRequest(
    string? Name,
    string? Description,
    decimal? Price,
    int? Quantity,
    Guid? CatelogyId);

